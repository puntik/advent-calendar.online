<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Request;

class FieldResource extends JsonResource
{

	/**
	 * Transform the resource into an array.
	 *
	 * @param  \Illuminate\Http\Request $request
	 *
	 * @return array
	 */
	public function toArray($request)
	{
		return [
			'id'          => $this->id,
			'calendar_id' => $this->calendar_id,
			'open_left'   => $this->open_limit - $this->users->count(),
			'open_my'     => $this->users->contains(Request::user()),
			'enable_at'   => $this->enable_at->format('Y-m-d'),
			'enabled'     => $this->enable_at < now(),
		];
	}
}
