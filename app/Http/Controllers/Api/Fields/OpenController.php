<?php

namespace App\Http\Controllers\Api\Fields;

use App\Http\Controllers\Controller;
use App\Http\Resources\FieldResource;
use App\Model\Entities\Calendar;
use App\Model\Entities\Field;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class OpenController extends Controller
{

	public function __invoke(
		Request $request,
		Calendar $calendar,
		Field $field
	) {
		/** @var \App\User $user */
		$user = $request->user();

		// check if field is openable
		if (! $field->isEnabled($user)) {
			return JsonResponse::create([], Response::HTTP_CONFLICT);
		}

		$field->users()->attach($user->id);

		return new FieldResource($field);
	}
}
