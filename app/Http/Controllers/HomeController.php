<?php

namespace App\Http\Controllers;

use App\Model\Entities\Calendar;

class HomeController extends Controller
{

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('auth');
	}

	/**
	 * Show the application dashboard.
	 *
	 * @return \Illuminate\Contracts\Support\Renderable
	 */
	public function index()
	{
		$calendar = Calendar::findOrFail(3);

		return view('home', compact('calendar'));
	}
}
