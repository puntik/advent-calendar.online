<?php

namespace App\Model\Entities;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Field extends Model
{

	protected $dates = [
		'enable_at',
	];

	protected $casts = [
		'calendar_id' => 'integer',
		'open_limit'  => 'integer',
	];

	protected $fillable = [
		'calendar_id',
		'enable_at',
		'open_limit',
	];

	// relationships

	public function calendar()
	{
		return $this->belongsTo(Calendar::class);
	}

	public function users()
	{
		return $this->belongsToMany(User::class);
	}

	// business

	public function isEnabled(User $user): bool
	{
		return ! ($this->enable_at > now() || $this->users->contains('id', $user->id));
	}
}
