<?php

namespace App\Model\Entities;

use Illuminate\Database\Eloquent\Model;

class Calendar extends Model
{

	protected $fillable = [
		'name',
		'slug',
	];
}
