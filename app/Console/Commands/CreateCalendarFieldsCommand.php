<?php

namespace App\Console\Commands;

use App\Model\Entities\Field;
use Illuminate\Console\Command;
use Illuminate\Support\Carbon;

class CreateCalendarFieldsCommand extends Command
{

	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'ac:create-fields 
		{calendar_id : Where to create fields}
		{start_at : When to start}
		{--open_limit=10 : Open limit}
		{--fields=24 : Fields}
		';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Command description';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle()
	{
		$calendarId = $this->argument('calendar_id');
		$openLimit  = $this->option('open_limit');
		$fields     = $this->option('fields');
		$startAt    = Carbon::parse($this->argument('start_at'))->setTime(0, 0);

		for ($i = 0; $i < $fields; $i++) {
			Field::create([
				'calendar_id' => $calendarId,
				'enable_at'   => $startAt,
				'open_limit'  => $openLimit,
			]);

			$startAt->addDay();
		}
	}
}
