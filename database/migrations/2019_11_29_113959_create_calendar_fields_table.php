<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCalendarFieldsTable extends Migration
{

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('fields', function (Blueprint $table) {
			$table->bigIncrements('id');

			$table->unsignedBigInteger('calendar_id');
			$table->timestamp('enable_at');
			$table->unsignedInteger('open_limit')->nullable();

			$table->foreign('calendar_id', 'fx_fields_calendar_id_calendars_id')->on('calendars')->references('id');

			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('fields');
	}
}
