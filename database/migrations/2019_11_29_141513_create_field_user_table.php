<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFieldUserTable extends Migration
{

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('field_user', function (Blueprint $table) {
			$table->bigIncrements('id');

			$table->unsignedBigInteger('user_id');
			$table->unsignedBigInteger('field_id');

			$table->foreign('user_id', 'fx_field_user_user_id_users_id')->references('id')->on('users');
			$table->foreign('field_id', 'fx_field_user_field_id_fields_id')->references('id')->on('fields');

			$table->unique(['field_id', 'user_id'], 'uq_field_user_field_id_user_id');

			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('field_user');
	}
}
