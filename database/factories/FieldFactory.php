<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use App\Model\Entities\Calendar;
use App\Model\Entities\Field;
use Faker\Generator as Faker;

$factory->define(Field::class, function (Faker $faker) {
	return [
		'calendar_id' => factory(Calendar::class)->create(),
		'enable_at'   => $faker->dateTimeThisMonth,
		'open_limit'  => 10,
	];
});
