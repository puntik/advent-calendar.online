<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use App\Model\Entities\Calendar;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

$factory->define(Calendar::class, function (Faker $faker) {

	$name = $faker->sentence;

	return [
		'name' => $name,
		'slug' => Str::slug($name),
	];
});
