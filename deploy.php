<?php

namespace Deployer;

require 'recipe/laravel.php';

// Project name
set('application', 'adventni-kalendar.online');

// Project repository
set('repository', 'git@bitbucket.org:puntik/advent-calendar.online.git');

// [Optional] Allocate tty for git clone. Default value is false.
set('git_tty', true);

// Shared files/dirs between deploys 
add('shared_files', [
	'database/database.sqlite',
]);
add('shared_dirs', []);

// Writable dirs by web server 
add('writable_dirs', []);

// Hosts

host('production')
	->stage('prod')
	->hostname('o')
	->set('deploy_path', '~/www/rem.cz/advent-calendar');

// Tasks

task('build', function () {
	run('cd {{release_path}} && build');
});

// [Optional] if deploy fails automatically unlock.
after('deploy:failed', 'deploy:unlock');

// Migrate database before symlink new release.

before('deploy:symlink', 'artisan:migrate');

