<?php

namespace Tests\Feature;

use App\Model\Entities\Calendar;
use App\Model\Entities\Field;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class ExampleTest extends TestCase
{

	/**
	 * @test
	 */
	public function it_fills_database()
	{
		$calendar = factory(Calendar::class)->create();
		factory(Field::class, 7)->create([
			'calendar_id' => $calendar->id,
		]);

		$this->assertTrue(true);
	}
}
